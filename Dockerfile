FROM oracle/graalvm-ce:19.0.0 as graalvm
COPY . /home/app/collatz-mn
WORKDIR /home/app/collatz-mn
RUN gu install native-image
RUN native-image --no-server -cp build/libs/collatz-mn-*.jar

FROM frolvlad/alpine-glibc
EXPOSE 8080
COPY --from=graalvm /home/app/collatz-mn .
CMD ["./collatz-mn"]
