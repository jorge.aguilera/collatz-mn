package collatz.mn;

import io.micronaut.core.util.ArrayUtils;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.Single;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.mitchtalmadge.asciidata.graph.ASCIIGraph;

@Controller("/")
public class CollatzController {

    @Get("{seed}")
    Single<String> collatz(long seed){
        return Single.create( emitter -> {

            Collatz collatz = new Collatz(seed, 3000);

            double[] seq = Stream.of(collatz.sequence).mapToDouble(Long::doubleValue).toArray();

            String graph = ASCIIGraph
                    .fromSeries(seq)
                    .withNumRows(50)
                    .withTickFormat(new DecimalFormat("#####0"))
                    .withTickWidth(12)
                    .plot();

            StringBuilder sb = new StringBuilder();
            sb.append("Collatz ")
                    .append(collatz.seed)
                    .append(", max:")
                    .append(collatz.max)
                    .append(", steps:")
                    .append(collatz.steps)
                    .append(", odds:")
                    .append(collatz.odd)
                    .append(", even:")
                    .append(collatz.even)
                    .append("\n")
                    .append(graph)
                    ;

            emitter.onSuccess(sb.toString());
        });
    }

}
