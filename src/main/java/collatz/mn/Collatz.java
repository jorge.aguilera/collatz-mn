package collatz.mn;

import java.util.ArrayList;
import java.util.List;

public class Collatz {

    public long seed;
    public long deep;
    public Long[] sequence;
    public long max;
    public long min;
    public int odd;
    public int even;
    public int steps;

    public Collatz(long seed, int deep){
        this.seed = seed;
        this.deep = deep;
        generateSequence();
    }

    private void generateSequence(){
        List<Long> ret = new ArrayList<>();
        if( seed == 1){
            ret.add(seed);
        }else{
            long n = seed;
            for(steps=0; steps< deep; steps++){
                if( n % 2 == 0){
                    n = n / 2;
                    odd++;
                }else{
                    n = (n*3)+1;
                    even++;
                }
                ret.add(n);
                max = Math.max(n,max);
                min = Math.min(n,min);
                if( n == 1)
                    break;
            }
        }
        sequence = ret.toArray(new Long[ret.size()]);
    }
}
