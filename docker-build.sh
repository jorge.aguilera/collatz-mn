#!/bin/sh
docker build . -t collatz-mn
echo
echo
echo "To run the docker container execute:"
echo "    $ docker run -rv --name collatz-mn -p 8080:8080 collatz-mn"
